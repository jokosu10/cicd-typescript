import supertest from 'supertest';

import app from './server';
const request = supertest(app);

it('Call the /youtube endpoint', async () => {
  const res = await request.get('/youtube');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Hello, youtube indonesia!');
});
it('Call the / endpoint', async () => {
  const res = await request.get('/');
  expect(res.status).toBe(200);
  expect(res.text).toBe('This App is running properly!');
});
it('Call the /pong endpoint', async () => {
  const res = await request.get('/ping');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Pong!');
});
it('Call the /hello/:name endpoint', async () => {
  const res = await request.get('/hello/Iqbal');
  expect(res.status).toBe(200);
  expect(res.body.message).toBe('Hello Iqbal');
});
it('Call the /iqbal endpoint', async () => {
  const res = await request.get('/iqbal');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Ini buatan iqbal!');
});
it('Call the /exabytes endpoint', async () => {
  const res = await request.get('/exabytes');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Halo Exabytes Indonesia!');
});
it('Call the /jokosu10 endpoint', async () => {
  const res = await request.get('/jokosu10');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Halo jokosu10, fork dari Ini buatan iqbal!');
});
