# create network envoy

docker network create envoy

# run envoy

cd cicd-typescript
docker run --name envoy -d -p 80:8080 -p 9901:9901 -v $(pwd)/envoy.yml:/etc/envoy/envoy.yaml --network envoy envoyproxy/envoy-alpine:v1.21-latest
